﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Services
{
    public interface IPostcodeAPI
    {
        Task<List<Postcode>> GetPostcode(string postcode);
    }
}
