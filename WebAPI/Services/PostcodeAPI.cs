﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class PostcodeAPI : IPostcodeAPI
    {
        private readonly HttpClient client;

        public PostcodeAPI(IHttpClientFactory clientFactory)
        {
            client = clientFactory.CreateClient("PublicPostcodeAPI");
        }

        public async Task<List<Postcode>> GetPostcode(string postcode)
        {
            var url = string.Format("/postcodes/{0}", postcode);
            var result = new List<Postcode>();
            var response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                var stringResponse = await response.Content.ReadAsStringAsync();

                result = JsonSerializer.Deserialize<List<Postcode>>(stringResponse,
                    new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
            }
            else
            {
                throw new HttpRequestException(response.ReasonPhrase);
            }

            return result;
        }
    }
}
