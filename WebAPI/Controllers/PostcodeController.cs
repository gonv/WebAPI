﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Models;
using WebAPI.Services;


namespace WebAPI.Controllers
{
    public class PostcodeController : Controller
    {
        private readonly IPostcodeAPI _postcodeAPI;

        public PostcodeController(IPostcodeAPI postcodeAPI)
        {
            _postcodeAPI = postcodeAPI;
        }

        public async Task<IActionResult> Index(string postcode)
        {
            List<Postcode> postcodes = new List<Postcode>();

            if (!string.IsNullOrEmpty(postcode))
            {
                postcodes = await _postcodeAPI.GetPostcode(postcode);
            }

            return View(postcodes);
        }
    }
}
