﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Search
    {
        public int SearchId { get; set; }
        public string Postcode { get; set; }
        public string Timestamp { get; set; }

    }
}
