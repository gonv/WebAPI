﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Postcode
    {
        public int status { get; set; }
        public string postcode { get; set; }
    }
}
